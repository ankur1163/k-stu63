const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require("cors");
const authRoute = require('./routes/auth')
const mongoose = require('mongoose');
const dotenv = require("dotenv");

dotenv.config();
const postRoute = require("./routes/posts");

const { request } = require('graphql-request');
 
app.use(express.json());
/* app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json()); */
app.use(cors());
app.use('/api/user',authRoute);
app.use("/api/user/posts",postRoute);
// app.post('/hello', function (req, res) {
//     res.send('POST request to the homepage')
//   })
app.listen(3000,()=>console.log("up and running"))

