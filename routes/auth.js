'use strict'

const router = require('express').Router();
const mongoose = require('mongoose');
const Userlogin = require('../model/User');
const bcrypt = require('bcryptjs');
const Usersignup = require('../model/Usersignup');
const jwt = require('jsonwebtoken');
const Joi = require('@hapi/joi');
const userRequest = require('../graphqlRequest/user');


const { client, request } = require('../graphqlRequest/client')

// 32 character string= thisisgoingtobereallygoodiknowitforlong
const createToken = async (user) => {
    console.log("user is",user)
    const tokenInfo = {
        "sub": user.id,
        "name": "John Doe",
        "https://hasura.io/jwt/claims": {
          "x-hasura-allowed-roles": ["admin"],
          "x-hasura-default-role": "admin",
          "x-hasura-user-id": user.id,
          "x-hasura-org-id": "Ankur",
          "x-hasura-custom": "custom-value"
        }
    }
    const token = await jwt.sign(tokenInfo, "thisisgoingtobereallygoodiknowit");
    return token;

}

router.post('/register', async (req, res) => {
    try {
        console.log("entered register")
        var salt = bcrypt.genSaltSync(10);
        const hash = await bcrypt.hashSync(req.body.password, salt)
        console.log("hash is",hash)
        const user = await userRequest.findUser(req.body.email);
        console.log("length",user.user_user);
        if(user.user_user.length===0){
            // create user here
            console.log("user not found")
          const signedupUser =  await userRequest.registerUser(req.body.email,hash); 
            console.log(signedupUser,"after signedup")
            const tokentosend = await createToken(signedupUser)
            console.log("tokentosend",tokentosend)
            //res.send(tokentosend)
            res.status(201).send({message: tokentosend});
        }
        else{
            console.log("user found")
            // return user also exist
        }        

    }
    catch (err) {
        console.log("error has come")
        res.send(400).status("error", err)

    }
})

router.post('/login', async (req, res) => {
    console.log("entered into login",req.body.email,req.body.password)
    try {
        console.log("this is nice")
        const user = await userRequest.findUser(req.body.email);
        console.log("user length")
        if(user.user_user.length===0){
            console.log("user not found")
            //return user does not exist
        }
        else {
            console.log(user.user_user[0].password,"and",req.body.password)
     
            const passwordCorrect = await bcrypt.compareSync(req.body.password, user.user_user[0].password);
            bcrypt.compare(req.body.password, user.user_user[0].password, function(err, res) {
                // res === true
                console.log("pp",res)
            });

            console.log("password is not there",passwordCorrect)
            if(!passwordCorrect){
                // return password mismatch
                console.log("password is incorrect")
                res.send(400).status("password is wrong")
            }
            else{
                console.log("password is correct",user.user_user[0])
                const tokentosend = await createToken(user.user_user[0])
                console.log("tokentosend",tokentosend)
                res.status(201).send({message: tokentosend});
                
                // return token as response
            }
        }
    } catch (err) {
        console.log("error is there")
        res.status(400).send(err)
    }
})




module.exports = router;
