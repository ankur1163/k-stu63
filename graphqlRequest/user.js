const client = require('./client') 

const c = client.client();

const UserFind = `
    query MyQuery($emailtocheck:String) {
        user_user(where: {email: {_eq: $emailtocheck}}){
          email,
          password,
          id
          }
      }`
// change the query to actual mutations
const UserRegister = `
      mutation MyMutation($emailtosave:String,$password:String) {
        insert_user_user(objects: {email: $emailtosave, password: $password}) {
          returning {
            email
            id
            role
          }
        }
      }
      `

const findUser = async (email) =>{
  console.log("entered into it",email)
  try {
    console.log("entered find user",email)
    const data = await c.request(UserFind, {emailtocheck:email})
    console.log("exiting fund user",data)
    return data
  }
  catch(err) {
    console.log("inside error finduser")
    throw TypeError("error in function call");
  }
 ;
}

const registerUser = async (email,password) =>{
  //include role as well
  try {
    const data = await c.request(UserRegister, {emailtosave:email,password:password})
    console.log("data",data.insert_user_user.returning[0])
    return data.insert_user_user.returning[0]
  }
  catch(err) {
    throw TypeError("error in function call");
  }
  console.log("email",email,"password",password)
  c.request(UserRegister, {emailtosave:email,password:password})
  .then(data => {
      console.log("returning data",data.insert_user_user.returning[0])
      return data;
    })
  .catch(err => {
    console.log(err.response.errors) // GraphQL response errors
    console.log(err.response.data) // Response data if available
  })
}

module.exports = {
    findUser,
    registerUser
}
